## References
- This is a college project for the course "CI1164 Introdução à Teoria da Computação" (Introduction to Scientific Computing), coursed in the Federal University of Paraná (UFPR)
- The course was ministred by Dr. [Daniel Weingaertner](https://web.inf.ufpr.br/danielw/) and Dr. [Armando Luiz N. Delgado](https://www.inf.ufpr.br/nicolui/)
- Developed by [Christian Debovi Paim](https://gitlab.com/christiandpo) and [Vinícius Teixeira](https://gitlab.c3sl.ufpr.br/vtvs18/icc)